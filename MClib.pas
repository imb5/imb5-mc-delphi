unit MClib;

interface

uses
  Logger, CmdLin, StdIni,
  imb5,
  System.SyncObjs,
  System.Classes,
  System.SysUtils,
  WinApi.Windows,
  System.Generics.Collections;

const
  icehMCParameterName = icehModelControlBase+1;
  icehMCParameterString = icehModelControlBase+2;
  icehMCParameterInt = icehModelControlBase+3;
  icehMCParameterFloat = icehModelControlBase+4;
  icehMCParameterBool = icehModelControlBase+5;
  icehMCParameterGUID = icehModelControlBase+6;
  icehMCParameterQuantity = icehModelControlBase+11;
  icehMCParameterValueUnit = icehModelControlBase+12;
  icehMCParameterUserUnit = icehModelControlBase+13;

  // model to node
  icehMCModelLinkID = icehModelControlBase+31; // guid
  icehMCModelID = icehModelControlBase+32; // guid
  //icehMCModelHeartbeat = icehModelControlBase+33; // guid

  // model
  icehMCModelName = icehModelControlBase+41; // string
  icehMCModelPrefix = icehModelControlBase+42; // string
  icehMCModelState = icehModelControlBase+46; // int32
  icehMCModelProgress = icehModelControlBase+47; // int32

  // node
  icehMCNodeID = icehModelControlBase+51; // guid
  icehMCNodeName = icehModelControlBase+52; // string
  icehMCNodeEventName = icehModelControlBase+53; // string
  // node folder
  icehMCNodeInquireFolder = icehModelControlBase+56; // string
  icehMCNodeFolder = icehModelControlBase+57; // string
  icehMCNodeSubFolder = icehModelControlBase+58; // string
  icehMCNodeFile = icehModelControlBase+59; // string
  // node model config
  icehMCNodeConfigPath = icehModelControlBase+61; // string
  icehMCNodeConfigRunCount = icehModelControlBase+62; // int

  mcPrefix = 'mc';

  mcModelsEventName = 'models';
  mcNodesEventName = 'nodes';

  mcMonitorModelsSleepTime = 60*1000; // msec

  // node -> starter command line switches
  mcNodeNameSwitch = 'NodeName';
  mcNodeIDSwitch = 'NodeID';
  mcLinkIDSwitch = 'LinkID';
  mcPrivateNodeEventNameSwitch = 'PrivateNodeEventName';



  // model states
  msRemoved = 1;
  msUninitialized = 2;
  msIdle = 3;
  msLocked = 4;
  msInitializing = 5;
  msReady = 6;
  msCalculating = 7;
  msBusy = 8;
  msNoKill = 16;

type
  TMCModel = class; // forward

  TMCParameter = class
  constructor Create(const aName: string);
  protected
    fName: string;
  public
    property name: string read fName;
  public
    procedure encode(var aPayload: TByteBuffer); virtual;
    procedure decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer); virtual;
  end;

  TMCParameterString = class(TMCParameter)
  constructor Create(const aName: string; const aValue: string);
  protected
    fValue: string;
  public
    property value: string read fValue write fValue;
  public
    procedure encode(var aPayload: TByteBuffer); override;
    procedure decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer); override;
  end;

  TMCParameterInt = class(TMCParameter)
  constructor Create(const aName: string; aValue: integer);
  protected
    fValue: integer;
  public
    property value: integer read fValue write fValue;
  public
    procedure encode(var aPayload: TByteBuffer); override;
    procedure decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer); override;
  end;

  TMCParameterFloat = class(TMCParameter)
  constructor Create(const aName: string; aValue: double; const aQuantity: string=''; const aValueUnit: string=''; const aUserUnit: string='');
  protected
    fValue: double;
    fQuantity: string;
    fValueUnit: string;
    fUserUnit: string;
  public
    property value: double read fValue write fValue;
    property quantity: string read fQuantity write fQuantity;
    property valueUnit: string read fValueUnit write fValueUnit;
    property userUnit: string read fUserUnit write fUserUnit;
  public
    procedure encode(var aPayload: TByteBuffer); override;
    procedure decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer); override;
  end;

  TMCParameterBool = class(TMCParameter)
  constructor Create(const aName: string; aValue: boolean);
  protected
    fValue: boolean;
  public
    property value: boolean read fValue write fValue;
  public
    procedure encode(var aPayload: TByteBuffer); override;
    procedure decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer); override;
  end;

  TMCParameterGUID = class(TMCParameter)
  constructor Create(const aName: string; const aValue: TGUID);
  protected
    fValue: TGUID;
  public
    property value: TGUID read fValue write fValue;
  public
    procedure encode(var aPayload: TByteBuffer); override;
    procedure decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer); override;
  end;

  TMCParameters = TObjectDictionary<string, TMCParameter>;

  //TMCModelCreate = reference to procedure(aModel: TMCModel);
  TMCModelRequestParameters = reference to procedure(aModel: TMCModel; aParameters: TMCParameters);
  TMCModelStart = reference to procedure(aModel: TMCModel; aParameters: TMCParameters);
  TMCModelStop = reference to procedure(aModel: TMCModel);
  TMCModelQuit = reference to procedure(aModel: TMCModel);

  TMCStarter = class; // forward

  TMCModel = class
  constructor Create(aStarter: TMCStarter; aConnection: TConnection); // const aLinkID: TGUID; const aModelName: string; aModelID: Integer; const aHub, aPort, aPrefix: string);
  destructor Destroy; override;
  private
    procedure setProgress(const aValue: integer);
    procedure setState(const aValue: integer);
  protected
    fStarter: TMCStarter; // ref to parent
    fConnection: TConnection; // owned, model connection
    fGlobalModelEvent: TEventEntry;
    fPrivateModelEvent: TEventEntry;
    fParameters: TMCParameters; // owned
    // state
    fState: Integer;
    fProgress: Integer;
    // actions
    fOnRequestParameters: TMCModelRequestParameters;
    fOnStartModel: TMCModelStart;
    fOnStopModel: TMCModelStop;
    fOnQuitModel: TMCModelQuit;
  protected
    procedure handleModelEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
    // signaling
    procedure signalModel(aEvent: TEventEntry);
    procedure signalModelUpdate();
    procedure signalModelState(aState: Integer);
    procedure signalModelProgress(aProgress: Integer);
    procedure signalModelExit();
  public
    property connection: TConnection read fConnection;
    property state: Integer read fState write setState;
    property progress: Integer read fProgress write setProgress;
    property globalModelEvent: TEventEntry read fGlobalModelEvent;
    property parameters: TMCParameters read fParameters;

    property OnRequestParameters: TMCModelRequestParameters read fOnRequestParameters write fOnRequestParameters;
    property OnStartModel: TMCModelStart read fOnStartModel write fOnStartModel;
    property OnStopModel: TMCModelStop read fOnStopModel write fOnStopModel;
    property OnQuitModel: TMCModelQuit read fOnQuitModel write fOnQuitModel;
  protected
    procedure setParameter(aParameter: TMCParameter); overload;
  public
    procedure setParameter(const aName: string; const aValue: string); overload;
    procedure setParameter(const aName: string; aValue: integer); overload;
    procedure setParameter(const aName: string; aValue: double); overload;
    procedure setParameter(const aName: string; aValue: boolean); overload;
    procedure setParameter(const aName: string; const aValue: TGUID); overload;
    procedure removeParameter(const aName: string);
    function isParameter(const aName: string): Boolean;
  end;

  TMCStarter = class
  constructor Create(aConnection: TConnection);
  destructor Destroy; override;
  protected
    // fRemmoteHost: string;
    // fRemotePort: string;
    fNodeName: string;
    fNodeID: TGUID;
    fPrivateNodeEventName: string;
    fLinkID: TGUID;
    fModels: TObjectDictionary<TGUID, TMCModel>; // owns models
    fConnection: TConnection; // owned, control connection
    fGlobalModelEvent: TEventEntry;
    fPrivateModelEvent: TEventEntry;
    fPrivateNodeEvent: TEventEntry;
    fMonitorThread: TThread;
    fMonitorQuit: TEvent;
    //fOnCreateModel: TMCModelCreate;
    function maxState: Integer;
    procedure handleModelEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
    procedure signalModelLinkID(const aLinkID: TGUID);
    procedure signalModelChildID(const aLinkID, aChildID: TGUID);
    procedure signalModelState(const aLinkID: TGUID; aState: Integer);
    procedure monitorModels;
  public
    property connection: TConnection read fConnection;
    property models: TObjectDictionary<TGUID, TMCModel> read fModels;
    //property OnCreateModel: TMCModelCreate read fOnCreateModel write fOnCreateModel;
  end;


function getComputerNameStr: string;

implementation

{ utils }

function getComputerNameStr: string;
var
  name: array [0 .. MAX_PATH] of char;
  s: DWORD;
begin
  s := SizeOf(name);
  if GetComputerName(name, s)
  then Result := name
  else Result := '';
end;


{ TMCParameter }

constructor TMCParameter.Create(const aName: string);
begin
 inherited Create;
 fName := aName;
end;

procedure TMCParameter.decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer);
begin
  case aKey of
    (icehMCParameterName shl 3) or wtLengthDelimited:
      fName := aPayload.bb_read_string(aCursor);
  else
    aPayload.bb_read_skip(aCursor, aKey and 7);
  end;
end;

procedure TMCParameter.encode(var aPayload: TByteBuffer);
begin
  aPayload := aPayload+
    TByteBuffer.bb_tag_string(icehMCParameterName, fName);
end;

{ TMCParameterString }

constructor TMCParameterString.Create(const aName, aValue: string);
begin
  inherited Create(aName);
  fValue := aValue;
end;

procedure TMCParameterString.decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer);
begin
  case aKey of
    (icehMCParameterString shl 3) or wtLengthDelimited:
      fValue := aPayload.bb_read_string(aCursor);
  else
    inherited;
  end;
end;

procedure TMCParameterString.encode(var aPayload: TByteBuffer);
begin
  aPayload := aPayload+
    TByteBuffer.bb_tag_string(icehMCParameterString, fValue);
  inherited;
end;

{ TMCParameterInt }

constructor TMCParameterInt.Create(const aName: string; aValue: integer);
begin
  inherited Create(aName);
  fValue := aValue;
end;

procedure TMCParameterInt.decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer);
begin
  case aKey of
    (icehMCParameterInt shl 3) or wtVarInt:
      fValue := aPayload.bb_read_int32(aCursor);
  else
    inherited;
  end;
end;

procedure TMCParameterInt.encode(var aPayload: TByteBuffer);
begin
  aPayload := aPayload+
    TByteBuffer.bb_tag_int32(icehMCParameterInt, fValue);
  inherited;
end;

{ TMCParameterFloat }

constructor TMCParameterFloat.Create(const aName: string; aValue: double; const aQuantity, aValueUnit, aUserUnit: string);
begin
  inherited Create(aName);
  fValue := aValue;
  fQuantity := aQuantity;
  fValueUnit := aValueUnit;
  fUserUnit := aUserUnit;
end;

procedure TMCParameterFloat.decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer);
begin
  case aKey of
    (icehMCParameterFloat shl 3) or wt64Bit:
      fValue := aPayload.bb_read_double(aCursor);
    (icehMCParameterQuantity shl 3) or wtLengthDelimited:
      fQuantity := aPayload.bb_read_string(aCursor);
    (icehMCParameterValueUnit shl 3) or wtLengthDelimited:
      fValueUnit := aPayload.bb_read_string(aCursor);
    (icehMCParameterUserUnit shl 3) or wtLengthDelimited:
      fUserUnit := aPayload.bb_read_string(aCursor);
  else
    inherited;
  end;
end;

procedure TMCParameterFloat.encode(var aPayload: TByteBuffer);
begin
  aPayload := aPayload+
    TByteBuffer.bb_tag_double(icehMCParameterFloat, fValue)+
    TByteBuffer.bb_tag_string(icehMCParameterQuantity, fQuantity)+
    TByteBuffer.bb_tag_string(icehMCParameterValueUnit, fValueUnit)+
    TByteBuffer.bb_tag_string(icehMCParameterUserUnit, fUserUnit);
  inherited;
end;

{ TMCParameterBool }

constructor TMCParameterBool.Create(const aName: string; aValue: boolean);
begin
  inherited Create(aName);
  fValue := aValue;
end;

procedure TMCParameterBool.decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer);
begin
  case aKey of
    (icehMCParameterString shl 3) or wtLengthDelimited:
      fValue := aPayload.bb_read_bool(aCursor);
  else
    inherited;
  end;
end;

procedure TMCParameterBool.encode(var aPayload: TByteBuffer);
begin
  aPayload := aPayload+
    TByteBuffer.bb_tag_bool(icehMCParameterInt, fValue);
  inherited;
end;

{ TMCParameterGUID }

constructor TMCParameterGUID.Create(const aName: string; const aValue: TGUID);
begin
  inherited Create(aName);
  fValue := aValue;
end;

procedure TMCParameterGUID.decode(aKey: UInt32; var aPayload: TByteBuffer; var aCursor: Integer);
begin
  case aKey of
    (icehMCParameterString shl 3) or wtLengthDelimited:
      fValue := aPayload.bb_read_guid(aCursor);
  else
    inherited;
  end;
end;

procedure TMCParameterGUID.encode(var aPayload: TByteBuffer);
begin
  aPayload := aPayload+
    TByteBuffer.bb_tag_guid(icehMCParameterInt, fValue);
  inherited;
end;

{ TMCModel }

procedure TMCModel.setParameter(const aName: string; aValue: boolean);
begin
  setParameter(TMCParameterBool.Create(aName, aValue));
end;

procedure TMCModel.setParameter(const aName: string; aValue: double);
begin
  setParameter(TMCParameterFloat.Create(aName, aValue));
end;

procedure TMCModel.setParameter(const aName: string; aValue: integer);
begin
  setParameter(TMCParameterInt.Create(aName, aValue));
end;

procedure TMCModel.setParameter(const aName, aValue: string);
begin
  setParameter(TMCParameterString.Create(aName, aValue));
end;

procedure TMCModel.setParameter(aParameter: TMCParameter);
begin
  TMonitor.Enter(parameters);
  try
    parameters.AddOrSetValue(aParameter.name, aParameter);
  finally
    TMonitor.Exit(parameters);
  end;
end;

procedure TMCModel.setParameter(const aName: string; const aValue: TGUID);
begin
  setParameter(TMCParameterGUID.Create(aName, aValue));
end;

constructor TMCModel.Create(aStarter: TMCStarter; aConnection: TConnection); // const aLinkID: TGUID; const aModelName: string; aModelID: Integer; const aHub, aPort, aPrefix: string);
begin
  inherited Create;
  fStarter := aStarter;
  fState := msUninitialized;
  fProgress := 0;
  fConnection := aConnection; // TSocketConnection.Create(aModelName, aModelID, aPrefix, aHub, aPort);
  fGlobalModelEvent := fConnection.eventEntry(mcModelsEventName).subscribe();
  fGlobalModelEvent.OnEvent.Add(handleModelEvent);
  fPrivateModelEvent := fConnection.eventEntry(fConnection.privateEventName, false).subscribe();
  fPrivateModelEvent.OnEvent.Add(handleModelEvent);
  signalModel(fGlobalModelEvent);
end;

destructor TMCModel.Destroy;
begin
  signalModelExit();
  FreeAndNil(fParameters);
  FreeAndNil(fConnection);
  inherited;
end;

procedure TMCModel.handleModelEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  fieldInfo: UInt32;
  ID: TGUID;
  returnEventName: string;
  filter: string;
  returnEvent: TEventEntry;
begin
  ID := TGUID.Empty;
  returnEventName := '';
  while aCursor<aLimit do
  begin
    fieldInfo := aBuffer.bb_read_uint32(aCursor);
    case fieldInfo of
      (icehObjectID shl 3) or wtLengthDelimited:
        begin
          ID := aBuffer.bb_read_guid(aCursor);
        end;
      (icehReturnEventName shl 3) or wtLengthDelimited:
        begin
          returnEventName := aBuffer.bb_read_string(aCursor);
        end;
      (icehObjectsInquire shl 3) or wtLengthDelimited:
        begin
          filter := aBuffer.bb_read_string(aCursor);
          // todo: implement request
          if returnEventName<>''
          then returnEvent := fConnection.eventEntry(returnEventName, false)
          else returnEvent := aEventEntry;
          try
            signalModel(returnEvent);
          finally
            if returnEventName<>''
            then returnEvent.Unpublish();
          end;
        end;
      // todo: implement
    else
      aBuffer.bb_read_skip(aCursor, fieldInfo and 7);
    end;
  end;
end;

function TMCModel.isParameter(const aName: string): Boolean;
begin
  TMonitor.Enter(parameters);
  try
    Result := parameters.ContainsKey(aName);
  finally
    TMonitor.Exit(parameters);
  end;
end;

procedure TMCModel.removeParameter(const aName: string);
begin
  TMonitor.Enter(parameters);
  try
    parameters.Remove(aName);
  finally
    TMonitor.Exit(parameters);
  end;
end;

procedure TMCModel.setProgress(const aValue: Integer);
begin
  if fProgress<>aValue then
  begin
    signalModelProgress(aValue);
    fProgress := aValue;
  end;
end;

procedure TMCModel.setState(const aValue: Integer);
begin
  if fState<>aValue then
  begin
    signalModelState(aValue);
    fState := aValue;
  end;
end;

procedure TMCModel.signalModelUpdate();
begin
  globalModelEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, connection.UniqueClientID)+
    TByteBuffer.bb_tag_string(icehMCModelPrefix, connection.Prefix)+
    TByteBuffer.bb_tag_int32(icehMCModelState, state)+
    TByteBuffer.bb_tag_int32(icehMCModelProgress, progress));
end;

procedure TMCModel.signalModelProgress(aProgress: Integer);
begin
  globalModelEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, connection.UniqueClientID)+
    TByteBuffer.bb_tag_int32(icehMCModelProgress, aProgress));

end;

procedure TMCModel.signalModel(aEvent: TEventEntry);
begin
  aEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, connection.UniqueClientID)+
    // node info
    TByteBuffer.bb_tag_guid(icehMCNodeID, fStarter.fNodeID)+
    TByteBuffer.bb_tag_string(icehMCNodeName, fStarter.fNodeName)+
    TByteBuffer.bb_tag_string(icehMCNodeEventName, fStarter.fPrivateNodeEventName)+
    // model
    TByteBuffer.bb_tag_string(icehMCModelName, connection.ModelName)+
    TByteBuffer.bb_tag_string(icehMCModelPrefix, connection.Prefix)+
    TByteBuffer.bb_tag_int32(icehMCModelState, state)+
    TByteBuffer.bb_tag_int32(icehMCModelProgress, progress));
end;

procedure TMCModel.signalModelState(aState: Integer);
begin
  globalModelEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, connection.UniqueClientID)+
    TByteBuffer.bb_tag_int32(icehMCModelState, aState));
end;

procedure TMCModel.signalModelExit();
begin
  globalModelEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehNoObjectID, connection.UniqueClientID));
end;

{ TMCStarter }

constructor TMCStarter.Create(aConnection: TConnection);
begin
  inherited Create;
  //fRemmoteHost := GetSetting(imbRemoteHostSwitch);
  //fRemotePort  := GetSetting(imbRemotePortSwitch);
  fNodeName := GetSetting(mcNodeNameSwitch, getComputerNameStr);
  fNodeID := TGUID.Create(GetSetting(mcNodeIDSwitch, TGUID.Empty.ToString));
  fPrivateNodeEventName := GetSetting(mcPrivateNodeEventNameSwitch);
  fLinkID := TGUID.Create(GetSetting(mcLinkIDSwitch, TGUID.NewGuid.ToString));
  fModels := TObjectDictionary<TGUID, TMCModel>.Create([doOwnsValues]);
  fConnection := aConnection; //TSocketConnection.Create(aModelName, 0, aPrefix, aHub, aPort);
  fGlobalModelEvent := fConnection.eventEntry(mcModelsEventName).subscribe();
  fGlobalModelEvent.OnEvent.Add(handleModelEvent);
  fPrivateModelEvent := fConnection.eventEntry(fConnection.privateEventName, false).subscribe();
  fPrivateModelEvent.OnEvent.Add(handleModelEvent);
  fPrivateNodeEvent := fConnection.eventEntry(fPrivateNodeEventName, false);
  signalModelLinkID(fLinkID);
  fMonitorQuit := TEvent.Create();
  fMonitorThread := TThread.CreateAnonymousThread(monitorModels);
  fMonitorThread.FreeOnTerminate := False;
  fMonitorThread.NameThreadForDebugging('TMCStarter.ModelMonitor');
  fMonitorThread.Start;
end;

destructor TMCStarter.Destroy;
begin
  fMonitorQuit.SetEvent;
  FreeAndNil(fMonitorThread);
  FreeAndNil(fMonitorQuit);
  FreeAndNil(fConnection);
  FreeAndNil(fModels);
  inherited;
end;

procedure TMCStarter.handleModelEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
begin
  // todo: implement
end;

function TMCStarter.maxState: Integer;
var
  child: TMCModel;
begin
  Result := msIdle;
  TMonitor.Enter(fModels);
  try
    for child in models.Values do
    begin
      if Result<child.state
      then Result := child.state;
    end;
  finally
    TMonitor.Exit(fModels);
  end;
end;

procedure TMCStarter.monitorModels;
begin
  while not TThread.CheckTerminated do
  begin
    try
      if fMonitorQuit.WaitFor(mcMonitorModelsSleepTime)=wrTimeout then
      begin
        signalModelState(fLinkID, maxState);
      end;
    except
      on E: Exception
      do Log.WriteLn('Exception in TMCStarter.monitorModels: '+e.Message, llError);
    end;
  end;
end;

procedure TMCStarter.signalModelChildID(const aLinkID, aChildID: TGUID);
begin
  fPrivateNodeEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, aLinkID)+
    TByteBuffer.bb_tag_guid(icehMCModelID, aChildID));
end;

procedure TMCStarter.signalModelLinkID(const aLinkID: TGUID);
begin
  fPrivateNodeEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, aLinkID)+
    TByteBuffer.bb_tag_guid(icehMCModelLinkID, fConnection.UniqueClientID));
end;

procedure TMCStarter.signalModelState(const aLinkID: TGUID; aState: Integer);
begin
  fPrivateNodeEvent.signalEvent(
    TByteBuffer.bb_tag_guid(icehObjectID, aLinkID)+
    TByteBuffer.bb_tag_int32(icehMCModelState, aState));
end;

end.